import React, { useEffect, useState } from "react";
import { IBasePokemon, IPokemon } from "../../shared/components/interface";
import "./Home.scss";
import ModalNew from "./newmodal/modal";
// import "./table.scss";

const pokemon = [
  { id: 1, nombre: "pikachu", imagen: "aa", ataque: "60", defensa: "20" },
  { id: 2, nombre: "charmander", imagen: "aa", ataque: "60", defensa: "20" },
  { id: 3, nombre: "bulbasor", imagen: "aa", ataque: "60", defensa: "20" },
];

const dateTable = [
  { title: "Nombre" },
  { title: "Imagen" },
  { title: "Ataque" },
  { title: "Defensa" },
  { title: "Acciones" },
];

const Home = () => {
  const [openModalNew, setOpenModalNew] = useState(false);
  const [datos, setDatos] = useState<IBasePokemon[]>([]);

  const openModal = () => setOpenModalNew(true);
  const newPokemon = () => {
    openModal();
  };

  const getData = async () => {
    const url =
      "https://maral-app-service-6ycinmd6na-uc.a.run.app/maral/admin/pokemon-jhornan-all";
    const result = await fetch(url);
    const getResult = await result.json();
    setDatos(getResult);
  };
  useEffect(() => {
    getData();
  }, []);

  const addPkemon = (newPokemon: IBasePokemon) => {
    const id = datos.length + 1;
    setDatos([...datos, { ...newPokemon }]);
  };

  const editPokemon = () => {
    openModal();
    console.log("abre");
  };

  const deletePokemon = (currentPokemon: IPokemon) => {
    // setDatos(datos.filter((i) => i.id !== currentPokemon.id));
  };

  return (
    <>
      <div className="container__total">
        <ul>
          <li className="active">
            <a>PokeDex</a>
          </li>
        </ul>
        <div className="container__title">
          <h3>Lista de Pokemons</h3>
        </div>
        <div className="container__form">
          <input
            className="container__form__search"
            type="text"
            placeholder="Buscar..."
          />
          <div>
            <button className="container__form__button" onClick={newPokemon}>
              Agregar Pokemon
            </button>
          </div>
        </div>
        <div className="container__table">
          <table>
            <thead>
              <tr>
                {dateTable.map((item, id) => {
                  return <th key={id}>{item.title}</th>;
                })}
              </tr>
            </thead>
            <tbody>
              {datos.map((pokemon, id) => (
                <tr key={id}>
                  <td>{pokemon.name}</td>
                  <td>
                    <img className="imagen" src={pokemon.url_picture} alt="" />
                  </td>
                  <td>{pokemon.attack}</td>
                  <td>{pokemon.defense}</td>
                  <td>
                    <button
                      className="container__table__edit"
                      // onClick={() => props.edit(pokemon.id)}
                    >
                      Editar
                    </button>
                    <button
                      className="container__table__remove"
                      // onClick={() => props.delete()}
                    >
                      Eliminar
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
      <ModalNew
        modal={openModalNew}
        setModal={setOpenModalNew}
        AddPkemon={addPkemon}
      />
    </>
  );
};

export default Home;
