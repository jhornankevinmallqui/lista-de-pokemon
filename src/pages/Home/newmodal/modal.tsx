import React from "react";
import { useState } from "react";
import { IBasePokemon, IPokemon } from "../../../shared/components/interface";
import "./modal.scss";

export interface ModalNewProps {
  modal: boolean;
  setModal: (visible: boolean) => void;
  AddPkemon: (pokemon: IBasePokemon) => void;
}

const NewModal = (props: ModalNewProps) => {
  const { modal, setModal, AddPkemon } = props;
  const [datos, setDatos] = useState({
    name: "",
    url_picture: "",
    attack: "",
    defense: "",
  });

  const Modal = () => {
    setModal(!modal);
  };

  const onFormSubmit = (e: any) => {
    e.preventDefault();
    AddPkemon(datos);
    Modal();
  };

  const onInputChange = (e: any) => {
    const { name, value } = e.target;
    setDatos({ ...datos, [name]: value });
  };

  return (
    <>
      {modal && (
        <div className="container__modal">
          <div className="container__modal__modalContent">
            <div>
              <h2 className="container__modal__modalContent__titulo">
                Nuevo Pokemon
              </h2>
            </div>
            <form onSubmit={onFormSubmit}>
              <div className="container__modal__modalContent__form">
                <div className="">
                  <div className="container__modal__modalContent__form__text">
                    <h5>Nombre:</h5>
                    <input type="text" name="nombre" onChange={onInputChange} />
                  </div>
                  <div className="container__modal__modalContent__form__text">
                    <h5>Imagen:</h5>
                    <input type="text" name="imagen" onChange={onInputChange} />
                  </div>
                </div>
                <div className="">
                  <div className="container__modal__modalContent__form__text">
                    <h5>Ataque</h5>
                    <h6>0</h6>
                    <input
                      type="range"
                      min="0"
                      max="100"
                      name="ataque"
                      onChange={onInputChange}
                    />
                    <h6>100</h6>
                  </div>
                  <div className="container__modal__modalContent__form__text">
                    <h5>Defensa</h5>
                    <h6>0</h6>
                    <input
                      type="range"
                      min="0"
                      max="100"
                      name="defensa"
                      onChange={onInputChange}
                    />
                    <h6>100</h6>
                  </div>
                </div>
              </div>
              <div className="container__modal__modalContent__botones">
                <button
                  className="container__modal__modalContent__botones__guardar"
                  type="submit"
                >
                  Guardar
                </button>
                <button
                  className="container__modal__modalContent__botones__cancelar"
                  onClick={Modal}
                >
                  Cancelar
                </button>
              </div>
            </form>
            <button className="container__modal__closeModal" onClick={Modal}>
              X
            </button>
          </div>
        </div>
      )}
    </>
  );
};

export default NewModal;
